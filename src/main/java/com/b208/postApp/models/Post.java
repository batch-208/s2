package com.b208.postApp.models;

import javax.persistence.*;

//Mark this Java Object as a representation of a database table via the use of @Entity
@Entity
//designate the table name that this model is connected to:
@Table(name="posts")
public class Post {
    //indicate that the following property is a primary key via @Id
    @Id
    //Auto-increment  the id property using @GeneratedValue
    @GeneratedValue
    private Long id;

    //Class properties that represent tabke columns in a relational database as @Column
    @Column
    private String title;

    @Column
    private String content;

    //Constructors and  Getters/Setters

    //default constructor needed when retrieving posts
    public Post(){

    }

    //parameterized constructor need when creating posts
    public Post(String title, String content){
        this.title = title;
        this.content = content;
    }

    public String getTitle(){
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content){
        this.content = content;
    }



}

